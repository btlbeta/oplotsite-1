module.exports = function(grunt) {
	
	grunt.initConfig({
		secret: grunt.file.readJSON('secret.json'),
		concat: {
		      js: {
		        src: ['src/js/jquery.js','src/js/bootstrap.min.js'],
		        dest: 'tmp/js/app.js',
		      }, 
		      css: {
		        src: ['src/css/bootstrap.min.css','tmp/css/style.min.css','src/css/font-awesome.min.css'],
		        dest: 'build/css/app.min.css',
		      }
		    },
		    uglify: {
		      js: {
		        files: {
		          'build/js/app.min.js': ['tmp/js/app.js']
		        }
		      }
	    },
	    copy: {
            dist: {
            
                files: [
                    	{	expand: true,
            		flatten: true,
            		filter: 'isFile', src: ['src/fonts/*'], dest: 'build/fonts' },
            		{	expand: true,
            		flatten: true,
            		filter: 'isFile', src: ['src/js/html.js'], dest: 'build/js' },
                    	
                ]
            },
        },
	    less: {
			dist: {
		        'files': {
		          'tmp/css/style.min.css': ['src/less/style.less']
		        },
		        'options': {
		          compress: true,
		          cleancss: false,
		          sourceMap: false
		        }
		      } // distfiles: {
	    }, //less
 		shell: {
	        deploy: { 
	            command: "ssh -p 187 <%= secret.username %>@<%= secret.host %> 'cd <%= secret.remotedir %>/<%= secret.host %>  && git pull && npm install && grunt build'"
	        }
	    },
	     autoprefixer: {
		    options: {
		      browsers: ['last 6 version', 'ie 8', 'ie 9']
		    },
		    // prefix the specified file
		    single_file: {
		      src: 'tmp/css/style.min.css',
		      dest: 'tmp/css/style.min.css',
		    }
		},
		  jinja: {
		    dist: {
		      files: [{
		        expand: true,
		        dest: 'build/',
		        cwd: 'templates/',
		        src: ['**/!(_)*.html']
		      }]
		    }
		  },
	    imagemin: {
	      options: {
	        cache: false
	      },
	      dist: {
	        files: [{
	          expand: true,
	          cwd: 'src/',
	          src: ['img/*.{png,jpg,gif}'],
	          dest: 'build/'
	        }]
	      }
	   } ,
	   lint5: {
		    dirPath: "build",
		    defaults: {
		        "email": "gunlinux@ya.ru",
		        "username": "gunlinux"
		      },
		      templates: [
		        "index.html"
		      ],
		      ignoreList: [
		       
		      ]
		    },
	    watch: {
	      gruntfile: {
	          files: ['Gruntfile.js'],
	          tasks: ['default']
	      },
	      templates: {
	        files: 'templates/**.html',
	        tasks: ['jinja'],
	      },
	      js: {
	        files: 'src/js/{,**/}*.js',
	        tasks: ['scripts']
	      },
	      less: {
	        files: 'src/less/style.less',
	        tasks: ['styles'],
	      },
	      img: {
	      	  files: 'src/img/{,**/}*',
	      	  tasks:['img']
	      },
			livereload: {
	          // Here we watch the files the sass task will compile to
	          // These files are sent to the live reload server after sass compiles to them
				options: { livereload: true },
				files: ['build/index.html','build/img/*','build/js/*','build/css/*'],
	        },
	    },
	    express: {
		    all: {
		        options: {
		            bases: ['<%= secret.localdir %>/build'],
		            port: '<%= secret.port %>',
		            hostname: "0.0.0.0",
		            livereload: true
		        }
	        }
	    }
	  }
  );

	require('load-grunt-tasks')(grunt);
	grunt.registerTask('img', ['newer:imagemin:dist']);
	grunt.registerTask('fonts', ['copy']);
	grunt.registerTask('template', ['jinja']);
	grunt.registerTask('styles', ['less', 'autoprefixer', 'concat:css']);
	grunt.registerTask('scripts', ['copy','concat:js','uglify:js']);
	grunt.registerTask('build', ['img','fonts','template','styles','scripts']);
	
	grunt.registerTask('default', ['build','express','watch']);
	grunt.registerTask('deploy', ['shell:deploy']);
};
